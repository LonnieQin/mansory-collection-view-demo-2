//
//  MasonryViewController.h
//  MansonryLayoutDemo
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LQMasonryViewLayout.h"
@interface MasonryViewController : UICollectionViewController<LQMasonryViewLayoutDelegate,UICollectionViewDelegateFlowLayout>

@end
