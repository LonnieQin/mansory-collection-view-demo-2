//
//  LQMasonryViewLayout.h
//  MansonryLayoutDemo
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LQMasonryViewLayout;
@protocol LQMasonryViewLayoutDelegate<NSObject>
- (CGFloat) collectionView:(UICollectionView*) collectionView
                    layout:(LQMasonryViewLayout*) layout
  heightForItemAtIndexPath:(NSIndexPath*) indexPath;
@end
@interface LQMasonryViewLayout : UICollectionViewFlowLayout
@property (nonatomic,assign) NSUInteger numberOfColumns;
@property (nonatomic,assign) CGFloat interItemSpacing;
@property (weak,nonatomic) IBOutlet id<LQMasonryViewLayoutDelegate> delegate;
@end
