//
//  MasonryViewController.m
//  MansonryLayoutDemo
//
//  Created by amttgroup on 15-5-18.
//  Copyright (c) 2015年 lonnie. All rights reserved.
//

#import "MasonryViewController.h"

@implementation MasonryViewController

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 100;
}

- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * CellIdentifier = @"cell";
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor redColor];
    return cell;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat randomHeight = 100 + (arc4random()%140);
    return CGSizeMake(100, randomHeight);
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(LQMasonryViewLayout *)layout heightForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat randomHeight = 100 + (arc4random() %140);
    return randomHeight;
}


@end
